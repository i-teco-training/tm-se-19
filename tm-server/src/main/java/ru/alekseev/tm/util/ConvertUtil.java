package ru.alekseev.tm.util;

import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.dto.ProjectDTO;
import ru.alekseev.tm.dto.SessionDTO;
import ru.alekseev.tm.dto.TaskDTO;
import ru.alekseev.tm.dto.UserDTO;
import ru.alekseev.tm.entity.Project;
import ru.alekseev.tm.entity.Session;
import ru.alekseev.tm.entity.Task;
import ru.alekseev.tm.entity.User;

public final class ConvertUtil {
    @Nullable
    public final static UserDTO convertUserToDto(@Nullable final User user) {
        if (user == null) return null;
        UserDTO userDTO = new UserDTO();
        userDTO.setEmail(user.getEmail());
        userDTO.setLogin(user.getLogin());
        userDTO.setPasswordHashcode(user.getPasswordHashcode());
        userDTO.setRoleType(user.getRoleType());
        userDTO.setId(user.getId());
        return userDTO;
    }

    @Nullable
    public final static User convertDtoToUser(@Nullable final UserDTO userDTO) {
        if (userDTO == null) return null;
        User user = new User();
        user.setId(userDTO.getId());
        user.setLogin(userDTO.getLogin());
        user.setPasswordHashcode(userDTO.getPasswordHashcode());
        user.setEmail(userDTO.getEmail());
        user.setRoleType(userDTO.getRoleType());
        return user;
    }

    @Nullable
    public final static ProjectDTO convertProjectToDto(@Nullable final Project project) {
        if (project == null) return null;
        ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setName(project.getName());
        projectDTO.setDescription(project.getDescription());
        projectDTO.setDateStart(project.getDateStart());
        projectDTO.setDateFinish(project.getDateFinish());
        projectDTO.setUserId(project.getUser().getId());
        projectDTO.setStatus(project.getStatus());
        projectDTO.setId(project.getId());
        projectDTO.setCreatedOn(project.getCreatedOn());
        return projectDTO;
    }

    @Nullable
    public final static Project convertDtoToProject(@Nullable final ProjectDTO projectDTO, @Nullable final User user) {
        if (projectDTO == null || user == null) return null;
        Project project = new Project();
        project.setName(projectDTO.getName());
        project.setDescription(projectDTO.getDescription());
        project.setDateStart(projectDTO.getDateStart());
        project.setDateFinish(projectDTO.getDateFinish());
        project.setUser(user);
        project.setStatus(projectDTO.getStatus());
        project.setId(projectDTO.getId());
        project.setCreatedOn(projectDTO.getCreatedOn());
        return project;
    }

    @Nullable
    public final static TaskDTO convertTaskToDto(@Nullable final Task task) {
        if (task == null) return null;
        TaskDTO taskDTO = new TaskDTO();
        taskDTO.setId(task.getId());
        taskDTO.setName(task.getName());
        taskDTO.setDescription(task.getDescription());
        taskDTO.setDateStart(task.getDateStart());
        taskDTO.setDateFinish(task.getDateFinish());
        taskDTO.setUserId(task.getUser().getId());
        taskDTO.setStatus(task.getStatus());
        taskDTO.setCreatedOn(task.getCreatedOn());
        if (task.getProject() == null) return taskDTO;
        taskDTO.setProjectId(task.getProject().getId());
        return taskDTO;
    }

    @Nullable
    public final static Task convertDtoToTask(
            @Nullable final TaskDTO taskDTO,
            @Nullable final User user,
            @Nullable final Project project
    ) {
        if (taskDTO == null || user == null || project == null) return null;
        Task task = new Task();
        task.setId(taskDTO.getId());
        task.setName(taskDTO.getName());
        task.setDescription(taskDTO.getDescription());
        task.setDateStart(taskDTO.getDateStart());
        task.setDateFinish(taskDTO.getDateFinish());
        task.setUser(user);
        task.setStatus(taskDTO.getStatus());
        task.setProject(project);
        task.setCreatedOn(taskDTO.getCreatedOn());
        return task;
    }

    @Nullable
    public final static SessionDTO convertSessionToDto(@Nullable final Session session) {
        if (session == null) return null;
        SessionDTO sessionDTO = new SessionDTO();
        sessionDTO.setId(session.getId());
        sessionDTO.setSignature(session.getSignature());
        sessionDTO.setTimestamp(session.getTimestamp());
        sessionDTO.setUserId(session.getUser().getId());
        return sessionDTO;
    }

    @Nullable
    public final static Session convertDtoToSession(@Nullable final SessionDTO sessionDTO, @Nullable final User user) {
        if (sessionDTO == null || user == null) return null;
        Session session = new Session();
        session.setSignature(sessionDTO.getSignature());
        session.setTimestamp(sessionDTO.getTimestamp());
        session.setUser(user);
        session.setId(sessionDTO.getId());
        return session;
    }
}
