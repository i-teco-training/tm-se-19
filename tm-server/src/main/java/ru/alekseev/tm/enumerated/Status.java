package ru.alekseev.tm.enumerated;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
@NoArgsConstructor
public enum Status {
    PLANNED("planned"),
    INPROGRESS("in progress"),
    READY("ready");

    private String displayName;

    Status(final String displayName) {
        this.displayName = displayName;
    }

    @Override
    @Nullable
    public final String toString() {
        return this.displayName;
    }

    @NotNull
    public static Status getStatusFromString(@Nullable final String displayName){
        for (@NotNull final Status status: Status.values()) {
            if (status.toString().equals(displayName))
                return status;
        }
        return PLANNED;
    }
}
