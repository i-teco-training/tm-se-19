package ru.alekseev.tm.api.iservice;

import ru.alekseev.tm.entity.Task;
import ru.alekseev.tm.entity.User;

import java.util.List;

public interface ITaskService extends IService<Task> {

    Task findOneByUserIdAndTaskId(String userId, String taskId);

    List<Task> findAllByUserId(String userId);

    void add(Task task);

    //void addTaskByUserIdTaskName(String userId, String taskName);

    void updateByNewData(User user, String taskId, String name);

    void delete(String id);

    void deleteByUserIdAndTaskId(String userId, String taskId);
}