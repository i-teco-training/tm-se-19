package ru.alekseev.tm.api.iservice;

import ru.alekseev.tm.entity.Session;

public interface ISessionService extends IService<Session>{

    void persist(Session session);

    boolean isValid(Session session);

    void delete(String id);
}
