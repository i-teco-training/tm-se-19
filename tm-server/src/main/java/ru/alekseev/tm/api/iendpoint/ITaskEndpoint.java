package ru.alekseev.tm.api.iendpoint;

import ru.alekseev.tm.dto.SessionDTO;
import ru.alekseev.tm.dto.TaskDTO;
import ru.alekseev.tm.entity.Session;
import ru.alekseev.tm.entity.Task;

import javax.jws.WebService;
import java.util.List;

@WebService
public interface ITaskEndpoint {

    TaskDTO findOneTaskByUserIdAndTaskId(String userId, String taskId);

    List<TaskDTO> findAllTasksByUserId(SessionDTO sessionDTO);

    void addTaskByUserIdTaskName(SessionDTO sessionDTO, String taskName);

    void updateTaskByTaskIdTaskName(SessionDTO sessionDTO, String taskId, String taskName);

    void deleteTask(String id);

    void deleteTaskByTaskId(SessionDTO sessionDTO, String taskId);
}
