package ru.alekseev.tm.api.iendpoint;

import ru.alekseev.tm.dto.Domain;
import ru.alekseev.tm.dto.SessionDTO;
import ru.alekseev.tm.entity.Session;

import javax.jws.WebService;

@WebService
public interface IDomainEndpoint {

    Domain getDomain(SessionDTO sessionDTO);

    void setDomain(SessionDTO sessionDTO, Domain domain);
}
