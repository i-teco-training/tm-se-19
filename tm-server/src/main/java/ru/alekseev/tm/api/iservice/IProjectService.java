package ru.alekseev.tm.api.iservice;

import ru.alekseev.tm.entity.Project;
import ru.alekseev.tm.entity.User;

import java.util.List;

public interface IProjectService extends IService<Project> {

    Project findOneByUserIdAndProjectId(String userId, String projectId);

    List<Project> findAllByUserId(String userId);

    void updateByUserProjectIdProjectName(User user, String projectId, String projectName);

    void deleteByProjectId(String userId, String projectId);
}
