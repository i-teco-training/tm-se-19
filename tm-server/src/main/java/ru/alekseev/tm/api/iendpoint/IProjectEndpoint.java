package ru.alekseev.tm.api.iendpoint;

import ru.alekseev.tm.dto.ProjectDTO;
import ru.alekseev.tm.dto.SessionDTO;

import javax.jws.WebService;
import java.util.List;

@WebService
public interface IProjectEndpoint {
    ProjectDTO findOneProjectByUserIdAndProjectId(String userId, String projectId);

    List<ProjectDTO> findAllProjectsByUserId(SessionDTO sessionDTO);

    void addProjectByUserIdProjectName(SessionDTO sessionDTO, String projectName);

    void updateProjectByProjectIdProjectName(SessionDTO sessionDTO, String projectId, String name);

    void deleteProjectByProjectId(SessionDTO sessionDTO, String projectId);
}
