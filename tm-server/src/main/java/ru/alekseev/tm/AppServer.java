package ru.alekseev.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.alekseev.tm.bootstrap.Bootstrap;
import ru.alekseev.tm.config.AppConfig;

public final class AppServer {
    public static void main(@Nullable String[] args) {
        @NotNull final ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
        @Nullable final Bootstrap bootstrap = context.getBean(Bootstrap.class);
        bootstrap.start();
    }
}
