package ru.alekseev.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.enumerated.Status;

import javax.persistence.*;
import java.util.Date;

@Setter
@Getter
@Entity
@Table(name = "app_task")
@NoArgsConstructor
public final class Task extends AbstractEntity {
    @Nullable
    private String name;

    @Nullable
    private String description;

    @Nullable
    @Column(name = "dateBegin")
    private Date dateStart;

    @Nullable
    @Column(name = "dateEnd")
    private Date dateFinish;

    @NotNull
    @Column(name = "task_status")
    @Enumerated(value = EnumType.STRING)
    private Status status = Status.PLANNED;

    @Nullable
    private Date createdOn;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne
    @JoinColumn(name = "project_id")
    private Project project;
}
