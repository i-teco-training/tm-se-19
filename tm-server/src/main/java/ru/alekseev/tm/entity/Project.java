package ru.alekseev.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.enumerated.Status;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Setter
@Getter
@Entity
@Table(name = "app_project")
@NoArgsConstructor
public final class Project extends AbstractEntity {
    @Nullable
    private String name;

    @Nullable
    private String description;

    @Nullable
    @Column(name = "dateBegin")
    private Date dateStart;

    @Nullable
    @Column(name = "dateEnd")
    private Date dateFinish;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "project_status")
    private Status status = Status.PLANNED;

    @Nullable
    private Date createdOn;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @OneToMany(mappedBy = "project", cascade = CascadeType.REMOVE, orphanRemoval = true)
    private List<Task> tasks;
}
