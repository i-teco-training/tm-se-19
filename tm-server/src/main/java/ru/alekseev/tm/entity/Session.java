package ru.alekseev.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;
import java.util.Date;

@Setter
@Getter
@Entity
@Table(name = "app_session")
@NoArgsConstructor
public final class Session extends AbstractEntity implements Cloneable {
    @NotNull
    private Long timestamp = new Date().getTime();

    @Nullable
    private String signature;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;
}
