package ru.alekseev.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.alekseev.tm.entity.Project;
import ru.alekseev.tm.entity.User;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, String> {

    User getOne(String id);

    User findByLoginAndPasswordHashcode(String login, String passwordHashcode);

    List<User> findAll();

    User save(User user);

    void deleteByLoginAndPasswordHashcode(String login, String passwordHashcode);

    void deleteAll();
}
