package ru.alekseev.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.alekseev.tm.entity.Project;

import java.util.List;

@Repository
public interface ProjectRepository extends JpaRepository<Project, String> {

    Project getOne(String id);

    Project findByUserIdAndId(String userId, String id);

    List<Project> findAll();

    List<Project> findAllByUserId(String userId);

    Project save(Project project);

    void deleteById(String id);

    void deleteAll();
}
