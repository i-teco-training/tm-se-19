package ru.alekseev.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.alekseev.tm.entity.Session;

@Repository
public interface SessionRepository extends JpaRepository<Session, String> {

    Session save(Session session);

    void deleteById(String id);
}
