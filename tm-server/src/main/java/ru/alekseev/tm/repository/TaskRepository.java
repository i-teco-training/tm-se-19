package ru.alekseev.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.alekseev.tm.entity.Task;

import java.util.List;

@Repository
public interface TaskRepository extends JpaRepository<Task, String> {

    Task getOne(String id);

    Task findByUserIdAndId(String userId, String id);

    List<Task> findAll();

    List<Task> findAllByUserId(String userId);

    Task save(Task task);

    void deleteById(String id);

    void deleteByUserIdAndId(String userId, String id);

    void deleteAll();
}
