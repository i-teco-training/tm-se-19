package ru.alekseev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.alekseev.tm.api.iservice.IDomainService;
import ru.alekseev.tm.dto.Domain;
import ru.alekseev.tm.entity.Project;
import ru.alekseev.tm.entity.Task;
import ru.alekseev.tm.entity.User;
import ru.alekseev.tm.repository.ProjectRepository;
import ru.alekseev.tm.repository.TaskRepository;
import ru.alekseev.tm.repository.UserRepository;

import java.util.List;

@Service
public final class DomainService implements IDomainService {
    @Nullable
    @Autowired
    private ProjectRepository projectRepository;
    @Nullable
    @Autowired
    private UserRepository userRepository;
    @Nullable
    @Autowired
    private TaskRepository taskRepository;

    @Override
    @Nullable
    public final Domain getDomain() {
        @Nullable final List<Project> projects = projectRepository.findAll();
        @Nullable final List<Task> tasks = taskRepository.findAll();
        @Nullable final List<User> users = userRepository.findAll();
        @NotNull final Domain domain = new Domain();
        domain.setProjects(projects);
        domain.setTasks(tasks);
        domain.setUsers(users);
        return domain;
    }

    @Override
    public final void setDomain(@Nullable final Domain domain) {
        @Nullable final List<Project> projects = domain.getProjects();
        @Nullable final List<Task> tasks = domain.getTasks();
        @Nullable final List<User> users = domain.getUsers();

        projectRepository.deleteAll();
        if (projects != null && projects.size() > 0) {
            for (Project project : projects)
                projectRepository.save(project);
        }
        taskRepository.deleteAll();
        if (tasks != null && tasks.size() > 0) {
            for (Task task : tasks)
                taskRepository.save(task);
        }
        userRepository.deleteAll();
        if (domain.getUsers() != null && domain.getUsers().size() > 0) {
            for (User user : users)
                userRepository.save(user);
        }
    }
}
