package ru.alekseev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.alekseev.tm.api.iservice.IUserService;
import ru.alekseev.tm.entity.User;
import ru.alekseev.tm.enumerated.RoleType;
import ru.alekseev.tm.repository.UserRepository;

@Service
public class UserService extends AbstractService<User> implements IUserService {
    @Nullable
    @Autowired
    private UserRepository userRepository;

    @Nullable
    public final User findOne(@NotNull final String id) {
        return userRepository.getOne(id);
    }

    @Override
    @Nullable
    public final User findOneByLoginAndPassword(@NotNull final String login, @NotNull final String passwordHashcode) {
        return userRepository.findByLoginAndPasswordHashcode(login, passwordHashcode);
    }

    @Override
    public final void addByLoginPasswordUserRole(
            @NotNull final String login,
            @NotNull final String passwordHashcode,
            @NotNull final RoleType roleType
    ) {
        if (login.isEmpty() || passwordHashcode.isEmpty() || roleType.toString().isEmpty()) return;
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHashcode(passwordHashcode);
        user.setRoleType(roleType);
        userRepository.save(user);
    }

    @Override
    public final void addByLoginAndPassword(
            @NotNull final String login,
            @NotNull final String passwordHashcode
    ) {
        if (login.isEmpty() || passwordHashcode.isEmpty()) return;
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHashcode(passwordHashcode);
        userRepository.save(user);
    }

    @Override
    public final void update(@NotNull final User user) {
        userRepository.save(user);
    }

    @Override
    public void updateUserPassword(@NotNull final String userId, @NotNull final String passwordHashcode) {
        @Nullable final User user = userRepository.getOne(userId);
        user.setPasswordHashcode(passwordHashcode);
        userRepository.save(user);
    }

    @Override
    public void updateUserByRole(String userId, RoleType roleType) {
        @Nullable final User user = userRepository.getOne(userId);
        user.setRoleType(roleType);
        userRepository.save(user);
    }

    @Override
    public final void deleteByLoginAndPassword(@NotNull final String login, @NotNull final String passwordHashcode) {
        userRepository.deleteByLoginAndPasswordHashcode(login, passwordHashcode);
    }
}
