package ru.alekseev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.alekseev.tm.api.iservice.ISessionService;
import ru.alekseev.tm.dto.SessionDTO;
import ru.alekseev.tm.entity.Session;
import ru.alekseev.tm.repository.SessionRepository;
import ru.alekseev.tm.util.ConvertUtil;
import ru.alekseev.tm.util.SignatureUtil;

@Service
public final class SessionService extends AbstractService<Session> implements ISessionService {
    @Nullable
    @Autowired
    private SessionRepository sessionRepository;

    @Override
    public final void persist(@NotNull final Session session) {
        sessionRepository.save(session);
    }

    @Override
    public final boolean isValid(@Nullable final Session session) {
        if (session == null) return false;
        if (session.getSignature() == null || session.getSignature().isEmpty()) return false;
        if (session.getUser() == null || session.getUser().getId().isEmpty()) return false;
        if (session.getTimestamp() == null) return false;

        @NotNull final String sourceSignature = session.getSignature();

        SessionDTO sessionDTO = ConvertUtil.convertSessionToDto(session);
        sessionDTO.setSignature(null);
        @NotNull final String targetSignature = SignatureUtil.sign(sessionDTO);
        return sourceSignature.equals(targetSignature);
    }

    public void delete(String id) {
        sessionRepository.deleteById(id);
    }
}
