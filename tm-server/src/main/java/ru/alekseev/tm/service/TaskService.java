package ru.alekseev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.alekseev.tm.api.iservice.ITaskService;
import ru.alekseev.tm.entity.Task;
import ru.alekseev.tm.entity.User;
import ru.alekseev.tm.repository.TaskRepository;

import java.util.List;

@Service
public class TaskService extends AbstractService<Task> implements ITaskService {
    @Nullable
    @Autowired
    private TaskRepository taskRepository;

    @Override
    @Nullable
    public final Task findOneByUserIdAndTaskId(@NotNull final String userId, @NotNull final String taskId) {
        return taskRepository.findByUserIdAndId(userId, taskId);
    }

    @Override
    @Nullable
    public final List<Task> findAllByUserId(@NotNull final String userId) {
        return taskRepository.findAllByUserId(userId);
    }

    @Override
    public final void add(@NotNull final Task task) {
        taskRepository.save(task);
    }

    @Override
    public final void updateByNewData(
            @NotNull final User user,
            @NotNull final String taskId,
            @NotNull final String name
    ) {
        if (user == null || taskId.isEmpty() || name.isEmpty()) return;
        Task task = taskRepository.getOne(taskId);
        task.setUser(user);
        task.setName(name);
        taskRepository.save(task);
    }

    @Override
    public final void delete(@NotNull final String id) {
        taskRepository.deleteById(id);
    }

    @Override
    public final void deleteByUserIdAndTaskId(@NotNull final String userId, @NotNull final String taskId) {
        if (userId.isEmpty() || taskId.isEmpty()) return;
        taskRepository.deleteByUserIdAndId(userId, taskId);
    }
}
