package ru.alekseev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.alekseev.tm.api.iservice.IProjectService;
import ru.alekseev.tm.entity.Project;
import ru.alekseev.tm.entity.User;
import ru.alekseev.tm.repository.ProjectRepository;

import java.util.List;

@Service
public final class ProjectService extends AbstractService<Project> implements IProjectService {
    @Nullable
    @Autowired
    private ProjectRepository projectRepository;

    @Override
    @Nullable
    public final Project findOneByUserIdAndProjectId(@NotNull final String userId, @NotNull final String projectId) {
        return projectRepository.findByUserIdAndId(userId, projectId);
    }

    @Override
    @Nullable
    public final List<Project> findAllByUserId(@NotNull final String userId) {
        if (userId.isEmpty()) return null;
        return projectRepository.findAllByUserId(userId);
    }

    @Override
    public final void add(@NotNull final Project project) {
        projectRepository.save(project);
    }

    @Override
    public final void updateByUserProjectIdProjectName(
            @NotNull final User user,
            @NotNull final String projectId,
            @NotNull final String projectName
    ) {
        if (user == null || projectId.isEmpty() || projectName.isEmpty()) return;
        Project project = projectRepository.getOne(projectId);
        project.setUser(user);
        project.setName(projectName);
        projectRepository.save(project);
    }

    @Override
    public final void delete(@NotNull final String id) {
        projectRepository.deleteById(id);
    }

    @Override
    public final void deleteByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        if (userId.isEmpty() || projectId.isEmpty()) return;
            @Nullable Project projectForExistenceChecking = projectRepository.getOne(projectId);
            if (projectForExistenceChecking.getUser() == null || projectForExistenceChecking.getUser().getId().isEmpty())
                return;
            if (!userId.equals(projectForExistenceChecking.getUser().getId())) return;
            projectRepository.deleteById(projectId);
    }
}
