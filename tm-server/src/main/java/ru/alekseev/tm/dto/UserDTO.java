package ru.alekseev.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.enumerated.RoleType;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Getter
@Setter
@NoArgsConstructor
public class UserDTO extends AbstractDTO {
    @Nullable
    private String login;

    @Nullable
    //@Column(name = "passwordHash")
    private String passwordHashcode;

    @NotNull
    //@Column(name = "role")
    //@Enumerated(value = EnumType.STRING)
    private RoleType roleType = RoleType.USER;

    @Nullable
    private String email;
}
