package ru.alekseev.tm;

import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import ru.alekseev.tm.config.AppConfig;
import ru.alekseev.tm.entity.User;
import ru.alekseev.tm.repository.UserRepository;
import ru.alekseev.tm.util.HashUtil;

import static org.junit.Assert.assertTrue;

@ContextConfiguration(classes=AppConfig.class)
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
public class AppServerTest
{
    @Nullable
    @Autowired
    private UserRepository userRepository;

    @Before
    @Ignore
    public void before() {
        System.out.println("[STARTING TEST]");
    }

    @After
    @Ignore
    public void after() {
        System.out.println("[FINISHING TEST]");
    }

    @Test
    @Ignore
    public void checkUser() {
        User user = userRepository.findByLoginAndPasswordHashcode("qqq", HashUtil.getMd5("qqq"));
        Assert.assertNotNull(user);
    }

    @Test
    @Ignore
    public void shouldAnswerWithTrue()
    {
        assertTrue( true );
    }
}
