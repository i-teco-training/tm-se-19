package ru.alekseev.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.alekseev.tm.api.ITerminalService;
import ru.alekseev.tm.api.ServiceLocator;
import ru.alekseev.tm.api.iendpoint.RoleType;
import ru.alekseev.tm.api.iendpoint.SessionDTO;
import ru.alekseev.tm.command.system.AbstractCommand;
import ru.alekseev.tm.endpoint.*;
import ru.alekseev.tm.service.TerminalService;

import java.util.*;

@Component
public final class Bootstrap implements ServiceLocator {
    @Nullable private SessionDTO currentSession;

    @Override
    public SessionDTO getCurrentSession() {
        return currentSession;
    }

    @Override
    public void setCurrentSession(SessionDTO currentSession) {
        this.currentSession = currentSession;
    }

    @Autowired
    @Nullable
    private ProjectEndpointService projectEndpointService;

    @Autowired
    @Nullable
    private TaskEndpointService taskEndpointService;

    @Autowired
    @Nullable
    private UserEndpointService userEndpointService;

    @Autowired
    @Nullable
    private SessionEndpointService sessionEndpointService;

    @Autowired
    @Nullable
    private DomainEndpointService domainEndpointService;

    @Autowired
    @Nullable
    private ITerminalService terminalService;

    @NotNull private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @Nullable private final Set<Class<? extends AbstractCommand>> classes =
            new Reflections("ru.alekseev.tm").getSubTypesOf(AbstractCommand.class);

    @Override
    @NotNull
    public final List<AbstractCommand> getCommands() {
        return new ArrayList<>(commands.values());
    }

    @Override
    @Nullable
    public DomainEndpointService getDomainEndpointService() {
        return domainEndpointService;
    }

    @Override
    @Nullable
    public final ProjectEndpointService getProjectEndpointService() {
        return projectEndpointService;
    }

    @Override
    @Nullable
    public final TaskEndpointService getTaskEndpointService() {
        return taskEndpointService;
    }

    @Override
    @Nullable
    public final UserEndpointService getUserEndpointService() {
        return userEndpointService;
    }

    @Override
    @Nullable
    public final SessionEndpointService getSessionEndpointService() {
        return sessionEndpointService;
    }

    @Override
    @Nullable
    public final ITerminalService getTerminalService() {
        return terminalService;
    }

    public final void registry(@NotNull final AbstractCommand command) {
        @NotNull final String commandName = command.getName();
        commands.put(commandName, command);
    }

    public final void start() throws Exception {
        if (classes == null) return;
        initCommands(classes);
        System.out.println("*** WELCOME TO TASK MANAGER ***");

        while (true) {
            @NotNull String commandName = terminalService.getFromConsole();
            if (!commands.containsKey(commandName)) {
                commandName = "help";
            }

            if (commands.get(commandName).isSecure() && currentSession == null) {
                System.out.println("LOGIN INTO PROJECT MANAGER");
                System.out.println();
                continue;
            }

            if (commands.get(commandName).isForAdminOnly()
                    && userEndpointService.getUserEndpointPort().findOneUser(currentSession).getRoleType() != RoleType.ADMIN) {
                System.out.println("ACCESS DENIED. YOU DON'T HAVE ADMINISTRATOR RIGHTS");
                System.out.println();
                continue;
            }

            try {
                commands.get(commandName).execute();
            } catch (@NotNull Exception e) {
                System.out.println("An exception was thrown");
                e.printStackTrace();
            }
            System.out.println();
        }
    }

    public final void initCommands(@NotNull final Set<Class<? extends AbstractCommand>> classes) throws Exception {
        for (@Nullable final Class commandClass : classes) {
            if (commandClass == null) continue;
            if (AbstractCommand.class.isAssignableFrom(commandClass)) {
                @NotNull final AbstractCommand command = (AbstractCommand) commandClass.newInstance();
                command.setServiceLocator(this);
                registry(command);
            }
        }
    }

}