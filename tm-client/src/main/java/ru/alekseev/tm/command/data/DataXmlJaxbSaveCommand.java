package ru.alekseev.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.api.iendpoint.Domain;
import ru.alekseev.tm.api.iendpoint.Session;
import ru.alekseev.tm.api.iendpoint.SessionDTO;
import ru.alekseev.tm.command.system.AbstractCommand;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.File;

public final class DataXmlJaxbSaveCommand extends AbstractCommand {
    @Override
    @NotNull
    public final String getName() {
        return "xml1-save";
    }

    @Override
    @NotNull
    public final String getDescription() {
        return "save to JAXB XML";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("[SAVE TO JAXB XML]");
        @Nullable final SessionDTO currentSession = serviceLocator.getCurrentSession();
        if (currentSession == null) return;
        if (currentSession.getUserId() == null || currentSession.getUserId().isEmpty()) return;

        System.out.println("ENTER SAVE PATH");
        @NotNull final String filePath = serviceLocator.getTerminalService().getFromConsole();
        if (filePath.isEmpty()) return;
        @Nullable final Domain domainToXml =
                serviceLocator.getDomainEndpointService().getDomainEndpointPort().getDomain(currentSession);
        if (domainToXml == null) return;

        @Nullable final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @Nullable final Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        @NotNull final File file = new File(filePath);
        marshaller.marshal(domainToXml, file);
        System.out.println("[DATA SAVED]");
    }

    @Override
    public final boolean isSecure() {
        return true;
    }

    @Override
    public final boolean isForAdminOnly() {
        return true;
    }
}