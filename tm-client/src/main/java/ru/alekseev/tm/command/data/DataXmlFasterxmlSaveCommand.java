package ru.alekseev.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.api.iendpoint.Domain;
import ru.alekseev.tm.api.iendpoint.IDomainEndpoint;
import ru.alekseev.tm.api.iendpoint.Session;
import ru.alekseev.tm.api.iendpoint.SessionDTO;
import ru.alekseev.tm.command.system.AbstractCommand;

import java.io.File;
import java.io.FileWriter;

public final class DataXmlFasterxmlSaveCommand extends AbstractCommand {
    @Override
    @NotNull
    public final String getName() {
        return "xml2-save";
    }

    @Override
    @NotNull
    public final String getDescription() {
        return "save to FasterXML XML";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("[SAVE TO FASTERXML XML]");
        @NotNull final IDomainEndpoint domainEndpoint = serviceLocator.getDomainEndpointService().getDomainEndpointPort();
        @Nullable final SessionDTO currentSession = serviceLocator.getCurrentSession();
        if (currentSession == null) return;
        if (currentSession.getUserId() == null || currentSession.getUserId().isEmpty()) return;

        System.out.println("ENTER SAVE PATH");
        @NotNull final String filePath = serviceLocator.getTerminalService().getFromConsole();
        if (filePath.isEmpty()) return;
        @Nullable final Domain domainToXml = domainEndpoint.getDomain(currentSession);
        if (domainToXml == null) return;

        @NotNull final ObjectMapper xmlMapper = new XmlMapper();
        @NotNull final String xmlString = xmlMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domainToXml);
        @NotNull final File xmlOutput = new File(filePath);
        @NotNull final FileWriter fileWriter = new FileWriter(xmlOutput);
        fileWriter.write(xmlString);
        fileWriter.close();

        System.out.println("[DATA SAVED]");
    }

    @Override
    public final boolean isSecure() {
        return true;
    }

    @Override
    public final boolean isForAdminOnly() {
        return true;
    }
}