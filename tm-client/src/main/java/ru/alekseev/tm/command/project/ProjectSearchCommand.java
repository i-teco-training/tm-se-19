package ru.alekseev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.api.iendpoint.*;
import ru.alekseev.tm.command.system.AbstractCommand;

import java.util.ArrayList;
import java.util.List;

public final class ProjectSearchCommand extends AbstractCommand {
    @Override
    @NotNull
    public final String getName() {
        return "search-projects";
    }

    @Override
    @NotNull
    public final String getDescription() {
        return "Search for projects";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("[SEARCH FOR PROJECTS]");
        @NotNull final IProjectEndpoint projectEndpoint =
                serviceLocator.getProjectEndpointService().getProjectEndpointPort();

        SessionDTO currentSession = serviceLocator.getCurrentSession();
        if (currentSession == null) return;
        if (currentSession.getUserId() == null || currentSession.getUserId().isEmpty()) return;

        @Nullable final List<ProjectDTO> listForSearching =
                projectEndpoint.findAllProjectsByUserId(currentSession);

        if (listForSearching.size() == 0) {
            System.out.println("LIST OF PROJECTS IS EMPTY");
            return;
        }
        System.out.println("ENTER SEARCH REQUEST");
        @NotNull String input = serviceLocator.getTerminalService().getFromConsole();
        if (input.isEmpty()) {
                System.out.println("Incorrect input.");
                return;
        }
        @NotNull List<ProjectDTO> resultList = new ArrayList<>();
        for (@Nullable final ProjectDTO project : listForSearching) {
            if (project == null) continue;
            @Nullable String name = project.getName();
            @Nullable String description = project.getDescription();
            if ((name != null && name.contains(input)) || (description != null && description.contains(input)))
                resultList.add(project);
        }
        if (resultList.size() == 0) {
            System.out.println("NO RESULTS");
            return;
        }
        for (int i = 0; i < resultList.size(); i++) {
            System.out.print(i + 1);
            System.out.println(") name:" + resultList.get(i).getName()
                    + ", projectId: " + resultList.get(i).getId()
                    + ", description: " + resultList.get(i).getDescription());
        }
    }

    @Override
    public final boolean isSecure() {
        return true;
    }
}