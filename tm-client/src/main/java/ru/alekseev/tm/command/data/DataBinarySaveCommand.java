package ru.alekseev.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.api.iendpoint.Domain;
import ru.alekseev.tm.api.iendpoint.IDomainEndpoint;
import ru.alekseev.tm.api.iendpoint.Session;
import ru.alekseev.tm.api.iendpoint.SessionDTO;
import ru.alekseev.tm.command.system.AbstractCommand;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;

public final class DataBinarySaveCommand extends AbstractCommand {
    @Override
    @NotNull
    public final String getName() {
        return "bin-save";
    }

    @Override
    @NotNull
    public final String getDescription() {
        return "Binary save";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("[BINARY SAVE]");
        @NotNull final IDomainEndpoint domainEndpoint =
                serviceLocator.getDomainEndpointService().getDomainEndpointPort();

        @Nullable final SessionDTO currentSession = serviceLocator.getCurrentSession();
        if (currentSession == null) return;
        if (currentSession.getUserId() == null || currentSession.getUserId().isEmpty()) return;

        System.out.println("ENTER SAVE PATH");
        @NotNull final String filePath = serviceLocator.getTerminalService().getFromConsole();
        if (filePath.isEmpty()) return;
        @Nullable final Domain domainForSerialization = domainEndpoint.getDomain(currentSession);
        if (domainForSerialization == null) return;

        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(filePath);
        @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.writeObject(domainForSerialization);
        fileOutputStream.close();
        objectOutputStream.close();

        System.out.println("[DATA SAVED]");
    }

    @Override
    public final boolean isSecure() {
        return true;
    }

    @Override
    public final boolean isForAdminOnly() {
        return true;
    }
}