package ru.alekseev.tm.command.data;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.api.iendpoint.Domain;
import ru.alekseev.tm.api.iendpoint.Session;
import ru.alekseev.tm.api.iendpoint.SessionDTO;
import ru.alekseev.tm.command.system.AbstractCommand;

import java.io.File;

public final class DataXmlFasterxmlLoadCommand extends AbstractCommand {
    @Override
    @NotNull
    public final String getName() {
        return "xml2-load";
    }

    @Override
    @NotNull
    public final String getDescription() {
        return "load from FasterXML XML";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("[LOAD FROM FASTERXML XML]");
        @Nullable final SessionDTO currentSession = serviceLocator.getCurrentSession();
        if (currentSession == null) return;
        if (currentSession.getUserId() == null || currentSession.getUserId().isEmpty()) return;

        System.out.println("ENTER PATH TO FILE");
        @NotNull final String filePath = serviceLocator.getTerminalService().getFromConsole();
        if (filePath.isEmpty()) return;

        @Nullable final File xmlFile = new File(filePath);
        @NotNull final XmlMapper xmlMapper = new XmlMapper();
        @Nullable final Domain loadedDomain = xmlMapper.readValue(xmlFile, Domain.class);
        if (loadedDomain == null) return;
        serviceLocator.getDomainEndpointService().getDomainEndpointPort().setDomain(currentSession, loadedDomain);
        System.out.println("[DATA LOADED]");
    }

    @Override
    public final boolean isSecure() {
        return true;
    }

    @Override
    public final boolean isForAdminOnly() {
        return true;
    }
}