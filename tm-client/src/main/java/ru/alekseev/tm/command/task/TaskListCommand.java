package ru.alekseev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.api.iendpoint.*;
import ru.alekseev.tm.command.system.AbstractCommand;

import java.util.List;

public final class TaskListCommand extends AbstractCommand {
    @Override
    @NotNull
    public final String getName() {
        return "show-tasks";
    }

    @Override
    @NotNull
    public final String getDescription() {
        return "Show all tasks";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("[LIST OF ALL TASKS]");
        @NotNull final ITaskEndpoint taskEndpoint =
                serviceLocator.getTaskEndpointService().getTaskEndpointPort();

        SessionDTO currentSession = serviceLocator.getCurrentSession();
        if (currentSession == null) return;
        if (currentSession.getUserId() == null || currentSession.getUserId().isEmpty()) return;

        @Nullable final List<TaskDTO> list = taskEndpoint.findAllTasksByUserId(currentSession);

        if (list.size() == 0) {
            System.out.println("LIST OF TASKS IS EMPTY");
        }
        for (int i = 0; i < list.size(); i++) {
            System.out.print(i + 1);
            System.out.println(") name:" + list.get(i).getName() + ", taskId: " + list.get(i).getId());
        }
    }

    @Override
    public final boolean isSecure() {
        return true;
    }
}
