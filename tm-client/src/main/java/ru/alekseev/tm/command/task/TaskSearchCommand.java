package ru.alekseev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.api.iendpoint.*;
import ru.alekseev.tm.command.system.AbstractCommand;

import java.util.ArrayList;
import java.util.List;

public final class TaskSearchCommand extends AbstractCommand {
    @Override
    @NotNull
    public final String getName() {
        return "search-tasks";
    }

    @Override
    @NotNull
    public final String getDescription() {
        return "Search for tasks";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("[SEARCH FOR TASKS]");
        @NotNull final ITaskEndpoint taskEndpoint =
                serviceLocator.getTaskEndpointService().getTaskEndpointPort();

        SessionDTO currentSession = serviceLocator.getCurrentSession();
        if (currentSession == null) return;
        if (currentSession.getUserId() == null || currentSession.getUserId().isEmpty()) return;

        @Nullable final List<TaskDTO> listForSearching = taskEndpoint.findAllTasksByUserId(currentSession);

        if (listForSearching.size() == 0) {
            System.out.println("LIST OF TASKS IS EMPTY");
            return;
        }
        System.out.println("ENTER SEARCH REQUEST");
        @NotNull String input = serviceLocator.getTerminalService().getFromConsole();
        if (input.isEmpty()) {
            System.out.println("Incorrect input.");
            return;
        }
        @NotNull List<TaskDTO> resultList = new ArrayList<>();
        for (@Nullable final TaskDTO task : listForSearching) {
            if (task == null) continue;
            @Nullable String name = task.getName();
            @Nullable String description = task.getDescription();
            if ((name != null && name.contains(input)) || (description != null && description.contains(input)))
                resultList.add(task);
        }
        if (resultList.size() == 0) {
            System.out.println("NO RESULTS");
            return;
        }
        for (int i = 0; i < resultList.size(); i++) {
            System.out.print(i + 1);
            System.out.println(") name:" + resultList.get(i).getName()
                    + ", taskId: " + resultList.get(i).getId()
                    + ", description: " + resultList.get(i).getDescription());
        }
    }

    @Override
    public final boolean isSecure() {
        return true;
    }
}