package ru.alekseev.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.api.iendpoint.Domain;
import ru.alekseev.tm.api.iendpoint.IDomainEndpoint;
import ru.alekseev.tm.api.iendpoint.Session;
import ru.alekseev.tm.api.iendpoint.SessionDTO;
import ru.alekseev.tm.command.system.AbstractCommand;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.File;

public final class DataJsonJaxbSaveCommand extends AbstractCommand {
    @Override
    @NotNull
    public final String getName() {
        return "json1-save";
    }

    @Override
    @NotNull
    public final String getDescription() {
        return "save to JAXB JSON";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("[SAVE TO JAXB JSON]");
        @NotNull final IDomainEndpoint domainEndpoint =
                serviceLocator.getDomainEndpointService().getDomainEndpointPort();
        @Nullable final SessionDTO currentSession = serviceLocator.getCurrentSession();
        if (currentSession == null) return;
        if (currentSession.getUserId() == null || currentSession.getUserId().isEmpty()) return;

        System.out.println("ENTER SAVE PATH");
        @NotNull final String filePath = serviceLocator.getTerminalService().getFromConsole();
        if (filePath.isEmpty()) return;
        @Nullable final Domain domainToJson = domainEndpoint.getDomain(currentSession);
        if (domainToJson == null) return;

        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        @NotNull final File file = new File(filePath);
        marshaller.marshal(domainToJson, file);

        System.out.println("[DATA SAVED]");
    }

    @Override
    public final boolean isSecure() {
        return true;
    }

    @Override
    public final boolean isForAdminOnly() {
        return true;
    }
}