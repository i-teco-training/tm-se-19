package ru.alekseev.tm.config;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ru.alekseev.tm.endpoint.*;

@Configuration
@ComponentScan(basePackages = "ru.alekseev.tm")
public class ClientConfig {
    @Bean
    public ProjectEndpointService projectEndpoint(){
        return new ProjectEndpointService();
    }

    @Bean
    public TaskEndpointService taskEndpoint(){
        return new TaskEndpointService();
    }

    @Bean
    public UserEndpointService userEndpoint(){
        return new UserEndpointService();
    }

    @Bean
    public SessionEndpointService sessionEndpoint(){
        return new SessionEndpointService();
    }

    @Bean
    public DomainEndpointService domainEndpoint(){
        return new DomainEndpointService();
    }
}
