package ru.alekseev.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.alekseev.tm.bootstrap.Bootstrap;
import ru.alekseev.tm.config.ClientConfig;

public final class AppClient {
    public static void main(@Nullable String[] args) throws Exception {
        @NotNull final ApplicationContext context = new AnnotationConfigApplicationContext(ClientConfig.class);
        @Nullable final Bootstrap bootstrap = context.getBean(Bootstrap.class);
        bootstrap.start();
    }
}
